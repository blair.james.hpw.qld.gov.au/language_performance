# ClojureScript Shadow-cljs Fibonacci Sequence microbenchmark

There are lies, damned lies, and there are microbenchmarks

## Project environment notes

The project was built in the following environment:

 - node manged with [nvm](https://github.com/nvm-sh/nvm)
 - node version 12 LTS via nvm command: nvm install lts/erbium && nvm alias default lts/erbium && nvm use default

Initial project creation with:

    npx create-cljs-project minimal-node-cli-fib-with-native-binary

Then basic app components added from: https://github.com/minimal-xyz/minimal-shadow-cljs-nodejs

Example adding shadow-cljs dev dependency:

    npm install --save-dev shadow-cljs

Build for live-dev/dev/release as follows:

    npx shadow-cljs watch app
    npx shadow-cljs compile app
    npx shadow-cljs release app

### Running locally after the release or compile

    node target/main.js

### Making a binary exectable (primarily for containerisation)

    npx pkg --targets node12-linux-x64 --output target/main target/main.js

Then running directly with:

    ./target/main

NOTE: The above native binary construction is for minimising container size and not necessarily for speed improvements.

