use num_bigint::BigUint;
use num_traits::{Zero, One};
use std::mem::replace;

// Calculate large fibonacci numbers.
fn fib(n: usize) -> BigUint {
    let mut f0: BigUint = Zero::zero();
    let mut f1: BigUint = One::one();
    for _ in 0..n {
        let f2 = f0 + &f1;
        // This is a low cost way of swapping f0 with f1 and f1 with f2.
        f0 = replace(&mut f1, f2);
    }
    f0
}

fn print_expo_str(n: BigUint) {
    let mut n_str = n.to_string();
    let mut n_str_deci = n_str.split_off(1);
    let expo = n_str_deci.len();
    n_str_deci.truncate(10);

    println!("{}.{}e+{}", n_str, n_str_deci, expo)
}

fn main() {
    print_expo_str(fib(1000000));
}
