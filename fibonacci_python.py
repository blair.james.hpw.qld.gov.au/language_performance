#!/usr/bin/env python3

def add(x: int, y: int):
    z = x + y
    return y, z

def main():
    i = 0
    x = 1
    y = 2
    for i in range(0, 1000001):
        x, y = add(x, y)
        add(x, y)
        i += 1

if __name__ == "__main__":
    main()

