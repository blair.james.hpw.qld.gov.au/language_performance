use apint::UInt;
use std::mem::replace;

fn fib(n: usize) -> UInt {
    let mut f0 = UInt::zero(1);
    let mut f1 = UInt::one(1);
    for _ in 0..n {
        let f2 = f0 + &f1;
        f0 = replace(&mut f1, f2);
    }
    f0
}

fn main() {
    println!("{}", Integer::fibonacci(100).to_string());
}

