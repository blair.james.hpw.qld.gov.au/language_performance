use ramp::Int;
use std::mem::replace;

// Calculate large fibonacci numbers.
fn fib(n: usize) -> Int {
    let mut f0 = Int::zero();
    let mut f1 = Int::one();
    for _ in 0..n {
        let f2 = f0 + &f1;
        // This is a low cost way of swapping f0 with f1 and f1 with f2.
        f0 = replace(&mut f1, f2);
    }
    f0
}

// FIXME: check if other ramp version fix the errors
fn main() {
    println!(fib(10));
}

