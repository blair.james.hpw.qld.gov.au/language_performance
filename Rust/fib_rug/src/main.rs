use rug::Integer;

fn print_expo_str(n: Integer) {
    let mut n_str = n.to_string();
    let mut n_str_deci = n_str.split_off(1);
    let expo = n_str_deci.len();
    n_str_deci.truncate(10);

    println!("{}.{}e+{}", n_str, n_str_deci, expo)
}

fn main() {
    print_expo_str(Integer::from(Integer::fibonacci(1000000)));
}
