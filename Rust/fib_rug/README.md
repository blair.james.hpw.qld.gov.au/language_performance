# Rust Fibonacci sequence microbenchmark (using apint)

Check the app before building the exec:

    cargo check

Build the app for dev:

    cargo build

Run the app after a dev build:

    ./target/debug/fib_*

Build and run the app for dev all-in-one:

    cargo run

Build the app for release:

    cargo build --release

Run the app for prod release benchmarking:

    ./target/release/fib_*

## Learning

Read Rust doco locally:

    rustup doc

Read doc related to the projects dependencies:

    cargo doc --open
