package main

import (
	"fmt"
    "math/big"
)

func main() {
	i := 0
	x := big.NewFloat(0)
	y := big.NewFloat(1)
	for i < 1000001 {
		if i == 1000000 {
			fmt.Printf("%g\n", x)
		}
		x.Add(x, y)
		x, y = y, x
		i++
	}
}
