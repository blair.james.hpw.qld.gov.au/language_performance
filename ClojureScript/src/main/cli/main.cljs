(ns cli.main)

(defn fib [n]
  (if (zero? n) 0
    (loop [a 0 b 1 n n]
      (if (= n 1) b
        (recur b (+ (js/BigInt a) (js/BigInt b)) (dec n)))))) 

(defn expo-str [in-str]
  (let [whole (first in-str)
        deci (rest in-str)
        expo-len (count deci)
        deci-trunc (take 10 deci)]
    (apply str [whole "." (apply str deci-trunc) "e+" expo-len])))

(defn main! []
  (let [n 1000000]
    (println (expo-str (.toString (fib n))))))
