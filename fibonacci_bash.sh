#!/bin/bash

add () {
    z=$(("$1" + "$2"))
    echo $z
}

main () {
    i=0
    x=0
    y=1
    z=0
    while [ $i -lt 1000001 ]; do
        ((i++))
        z=$(add $x $y)
        x=$y
        y=$z
    done
}

main
